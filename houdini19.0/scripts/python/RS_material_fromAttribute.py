import hou
import random
import re
def main():
    # If the active pane is not a scene viewer, raise an error

    sel = hou.selectedNodes()
    if sel:
        node = hou.selectedNodes()[0]
        if node.type().category() == hou.sopNodeTypeCategory():

            if node.geometry().findPrimAttrib('shop_materialpath'):

                shop_materialpath_atrr = node.geometry().primStringAttribValues('shop_materialpath')
                materials =  list(set(shop_materialpath_atrr))

                # Get the last Part and remove empty Materials
                materials = [x for x in materials if x !=""]

                matnet = node.parent().createNode('matnet',node_name="matnet",run_init_scripts=False)

            
                materialNode = node.parent().createNode('material')
                materialNode.setFirstInput(node)
                materialNode.setDisplayFlag(True)
                materialNode.setRenderFlag(True)

                #matnet.moveToGoodPosition()
                materialNode.moveToGoodPosition()


                materialNode.parm('num_materials').set(len(materials))
                for i in range(len(materials)):

                    matname = materials[i].rsplit("/", 1)
                    if len(matname)>1:
                        matname = matname[1]
                    else:
                        matname = matname[0]


                    matname = re.sub('[^a-zA-Z0-9-_*.]', '_', matname)
                    print(matname)
                    # Create and Connect RS Material Builder
                    redshift_vopnet = matnet.createNode('redshift_vopnet',node_name=node.name(),run_init_scripts=True)
                    rsmaterial = hou.node(str(redshift_vopnet.path())+"/Material1")

                    
                    # add random diffuse value
                    color = [0,0,0]

                    for j in range(len(color)):
                        color[j] = random.uniform(0.3,1)

                    rsmaterial.parmTuple('diffuse_color').set(color)





                    redshift_vopnet.moveToGoodPosition()

                    materialNode.parm('group'+str(i+1)).set("@shop_materialpath="+str(materials[i]))

                    materialNode.parm('shop_materialpath'+str(i+1)).set("../"+matnet.name()+"/"+redshift_vopnet.name())



