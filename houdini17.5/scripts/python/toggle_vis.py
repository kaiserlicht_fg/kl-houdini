import hou

def main():
 
    selNodes = hou.selectedNodes()
    
    if not selNodes:
        hou.ui.displayMessage("Nothing is selected")
        return False
     
    else:        

        for nodes in selNodes:
            if nodes.isDisplayFlagSet():
                nodes.setDisplayFlag(False)
            else:
                nodes.setDisplayFlag(True)
             
main()