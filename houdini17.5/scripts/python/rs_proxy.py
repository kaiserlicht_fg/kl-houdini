import hou

path = "$HIP/rs/"

def export_rs_proxy(selectedNode,path):


    if selectedNode.type().name() == "geo":

        enableProxy = selectedNode.parm('RS_objprop_proxy_enable')

        # Get the active Render Node
        renderflag = hou.nodeFlag.Render
        rendernode = [node for node in selectedNode.children() if node.isGenericFlagSet(renderflag)][0]

        # Check if a proxy setup is already present
        rs_proxyoutput_node = False

        for childnode in selectedNode.children():
            if childnode.type().name() == 'Redshift_Proxy_Output':
                rs_proxyoutput_node = childnode
                break

        if not rs_proxyoutput_node:

            # Create A Proxy Export node
            rs_proxyoutput_node = rendernode.createOutputNode('Redshift_Proxy_Output')


            # Create A Switch to switch between original geo and the rs proxySOP
            switchnode = rendernode.createOutputNode('switch','proxy_switch')
            switchnode.setDisplayFlag(True)
            switchnode.setRenderFlag(True)

            # Conncet the rs proxySOP
            proxySOP_node = switchnode.createInputNode(1,'redshift_proxySOP')
            proxySOP_node.moveToGoodPosition()

            # Channel Reference the output to the geometry proxy parameter
            proxySOP_path = rs_proxyoutput_node.parm('RS_archive_file')
            geo_proxy_path = selectedNode.parm('RS_objprop_proxy_file')
            geo_proxy_path.deleteAllKeyframes()
            geo_proxy_path.set(proxySOP_path)
            geo_proxy_path.expression()


            # Channel Reference Enable Proxy to the Switch
            switchnode_switch = switchnode.parm('input')
            switchnode_switch.set(enableProxy)
            switchnode_switch.expression()

            
        # expand environment variables to make sure we point to the correct file even if hip changes
        hip = hou.getenv("HIP")
        path = path.replace("$HIP",hip)
        
        # set filename
        filename = selectedNode.path()
        filename = filename.replace("/obj/","")
        filename = filename.replace("/","_")
        path = path + filename+".rs"

        # DISABLE THIS IF YOU WANT TO USE DEFAULT PATH OF THE NODE
        rs_proxyoutput_node.parm("RS_archive_file").set(path)            
            
        # Export the Proxy

        if enableProxy.eval() == True:
            enableProxy.set(False)

        # Disable Object Based Transformations
        rs_proxyoutput_node.parm('RS_archive_noXform').set(True)
        hou.parm(rs_proxyoutput_node.path()+'/execute').pressButton()
        enableProxy.set(True)


def main(path):

    num_tasks = xrange(len(hou.selectedNodes()))

    with hou.InterruptableOperation("Exporting RS Proxy", open_interrupt_dialog=True) as operation:
        for i in num_tasks:            
            export_rs_proxy(hou.selectedNodes()[i],path)            
            overall_percent = float(i) / float(len(hou.selectedNodes()))
            operation.updateProgress(overall_percent) 




main(path)