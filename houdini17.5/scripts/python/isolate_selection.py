import hou

def findList():
 
    selNodes = hou.selectedNodes()

    otherList = []
    
    if not selNodes:
        hou.ui.displayMessage("Nothing is selected")
        return False
     
    if selNodes:        
        
        # Get All Nodes of certain Types
        typelist = ["geo","instance"]

        for type in typelist:
            type = hou.nodeType(hou.objNodeTypeCategory(),type) 
            instances = list(type.instances())
            otherList += instances

                    
    if not otherList:
        hou.ui.displayMessage("Object(s) already isolated")
        return False
        

    subnetnodes = []
    for node in selNodes:   
        node.setDisplayFlag(True)
        
        # Check if selected Node is a Subnet
        
        if node.childTypeCategory() == node.type().category():
            for subnode in node.allSubChildren():
                # Add all Geo nodes inside a selected subnet   
                if subnode.type().name() in typelist:
                    subnetnodes.append(subnode)
                    
    
    subnetTuple = tuple(subnetnodes)
    selNodes+=subnetTuple
    
    otherTuple = tuple(otherList)
        
    ISODATA = {"selectedNodes" : selNodes, "otherNodes" : otherTuple }
    return ISODATA

def main():    
    try :
        hou.session.ISOMODE
    except:
        hou.session.ISOMODE = ""
    
    if hou.session.ISOMODE == "SHOW": 
        
        for n in hou.session.ISOLATED:
            try:
                n = hou.node(n)
                n.setDisplayFlag(True)
            except:
                continue
        hou.session.ISOMODE = ""            
    else:
        list = findList()
        filteredlist = []
        if list:
            for i in list["otherNodes"]:
            
                if "hlight" in i.type().name() or "rslight" in i.type().name():
                    pass
                else:
                    if i not in list["selectedNodes"] and i.isDisplayFlagSet():
                    
                        try:
                            i.setDisplayFlag(False)
                            filteredlist.append(i)
                        except:
                            pass
        stringlist = str([x.path() for x in filteredlist])
        hou.appendSessionModuleSource("ISOLATED = "+stringlist)
        hou.appendSessionModuleSource("ISOMODE = 'SHOW'")

main()